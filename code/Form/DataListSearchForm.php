<?php
class DataListSearchForm extends Form {
	private static $page_length = 10;
	private static $allowed_export = true;
	
    protected $extraClasses = array('datalist-search' => 'datalist-search');
    /**
     * @var int $pageLength How many results are shown per page.
     * Relies on pagination being implemented in the search results template.
     */
    protected $pageLength;
	protected $allowedExport;
    protected $sourceFilter;
	protected $sourceLimit;
    protected $sourceSort;
    protected $summaryFieldList;
    protected $summaryTitle;
	
    /**
     * 
     * @param Controller $controller
     * @param string $name The name of the form (used in URL addressing)
     * @param FieldList $fields Optional, defaults to a single field named "Search". Search logic needs to be customized
     *  if fields are added to the form.
     * @param FieldList $actions Optional, defaults to a single field named "Go".
     */
    public function __construct($controller, $name, $sourceClass, $sourceFilter, $fields = null, $actions = null) {
        if(!$fields) {
            $fields = FieldList::create(
                TextField::create('Search', _t('DataListSearchForm.SEARCH', 'Search'))
            );
        }
        
        if(!$actions) {
            $actions = FieldList::create(
                FormAction::create("doSearch", _t('DataListSearchForm.BUTTONSEARCH', 'Search'))
            );
        }
        
        $dataField = DataListField::create('DataList', $sourceClass);
        $fields->push($dataField);
        
        $this->sourceFilter = $sourceFilter;
        
        parent::__construct($controller, $name, $fields, $actions);
        
        Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
        Requirements::add_i18n_javascript(SAPPHIRE_DIR . '/javascript/lang');
        Requirements::javascript('datalist/javascript/DataListField.js');
        Requirements::css('datalist/css/DataListField.css');
        
		$this->setPageLength($this->config()->get('page_length'));
		$this->setAllowedExport($this->config()->get('allowed_export'));

        $data = $this->getLoadData();
        if($data){
            $this->loadDataFrom($data);
        }
    }
    
    public function getDataField(){
        return $this->Fields()->dataFieldByName('DataList');
    }
    
    public function getDataClass(){
        return $this->Fields()->dataFieldByName('DataList')->sourceClass();
    }
    
    public function setDataFieldList($field_list){
        $this->getDataField()->setFieldList($field_list);
        return $this;
    }
    
    public function setFieldCasting($field_casting){
        $this->getDataField()->setFieldCasting($field_casting);
        return $this;
    }
    
    public function setFieldFormatting($field_formatting){
        $this->getDataField()->setFieldFormatting($field_formatting);
        return $this;
    }
    
    public function setCsvFieldFormatting($field_formatting){
        $this->getDataField()->setCsvFieldFormatting($field_formatting);
        return $this;
    }
    
    public function doSearch($data, $form){
    	$fields = $this->VisibleFields();
		$loadData = array();
        $searchData = array();
        if(sizeof($fields)){
            $obj = singleton($this->getDataField()->sourceClass());
            foreach($fields as $field){
            	if($field->isReadonly() || $field->isDisabled()){
            		continue;
            	}
            	if(($fieldPos = strpos($field->getName(), '__'))!==false) {
					$relation = substr($field->getName(), 0, $fieldPos);
					$relation_class = $obj->getRelationClass($relation);
					if($relation_class && $dbfield = singleton($relation_class)->dbObject(substr($field->getName(), $fieldPos + 2))){
						$loadData[$field->getName()] = $field->Value();
						$searchData[str_replace('__', '.', $field->getName())] = array('value' => $field->dataValue(), 'data_type' => $dbfield->class, 'field_type' => $field->class);
					}
				}
				else {
	                if($dbfield = $obj->dbObject($field->getName())) {
	                	$loadData[$field->getName()] = $field->Value();
	                    $searchData[$field->getName()] = array('value' => $field->dataValue(), 'data_type' => $dbfield->class, 'field_type' => $field->class);
	                }
				}
            }
        }
		
		$this->setLoadData($loadData);
		$this->setSearchData($searchData);
        return $this->controller->redirectBack();
    }

	public function setLoadData($data){
		Session::clear("SearchFormInfo.{$this->getController()->ClassName}.{$this->FormName()}.loadData");
        Session::set("SearchFormInfo.{$this->getController()->ClassName}.{$this->FormName()}.loadData", $data);
		return $this;
	}
    
    public function getLoadData(){
        return Session::get("SearchFormInfo.{$this->getController()->ClassName}.{$this->FormName()}.loadData");
    }
	
	public function setSearchData($data){
		Session::clear("SearchFormInfo.{$this->getController()->ClassName}.{$this->FormName()}.searchData");
        Session::set("SearchFormInfo.{$this->getController()->ClassName}.{$this->FormName()}.searchData", $data);
		return $this;
	}
    
    public function getSearchData(){
        return Session::get("SearchFormInfo.{$this->getController()->ClassName}.{$this->FormName()}.searchData");;
    }
	
	public function getDataList(){
		$data = $this->getSearchData();
        $searchFilter = array();
		$dateRangeFilter = array();
        if(sizeof($data)){
            foreach($data as $name => $field){
                if($field['value'] != '') {
                    if($field['data_type'] == 'Date' || $field['data_type'] == 'SS_Datetime' || $field['data_type'] == 'Datetime'){
                    	if($field['field_type'] == 'DateRangeField'){
                    		$date = new Date();
	                        $date->setValue($field['value']['_Start']);
                    		$searchFilter[sprintf('%s:GreaterThanOrEqual', $name)] = $date->URLDate() .' 00:00:00';
							$date = new Date();
	                        $date->setValue($field['value']['_End']);
							$searchFilter[sprintf('%s:LessThanOrEqual', $name)] = $date->URLDate() .' 23:59:59';
                    	} else {
	                        $date = new Date();
	                        $date->setValue($field['value']);
	                        $searchFilter[sprintf('%s:StartsWith', $name)] = $date->URLDate();
						}
                    }
                    else{
                        $searchFilter[$name] = $field['value'];
                    }
                }
            }
        }

        $datalist = DataList::create($this->getDataClass())->filter(array_merge($searchFilter, $this->sourceFilter));
		if($limit = $this->getSourceLimit()){
			$datalist = $datalist->limit($limit);
		}
        
        if($sort = $this->getSourceSort()){
            $datalist = $datalist->sort($sort);
        }
		return $datalist;
	}

    public function getResults(){
        return $this->getDataField()->setCustomQuery($this->getDataList())->FieldHolder();
    }
	
	/**
     * Set the limit number of records.
     * 
     * @param int $limit
     */
    public function setSourceLimit($limit) {
        $this->sourceLimit = $limit;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getSourceLimit() {
        return $this->sourceLimit;
    }
    
    /**
     * Set the maximum number of records shown on each page.
     * 
     * @param int $length
     */
    public function setPageLength($length) {
        $this->pageLength = $length;
		$this->getDataField()->setPageSize($length);
        return $this;
    }
    
    /**
     * @return int
     */
    public function getPageLength() {
        return $this->pageLength;
    }
	
	/**
     * Set the maximum number of records shown on each page.
     * 
     * @param int $length
     */
    public function setAllowedExport($allowed) {
        $this->allowedExport = $allowed;
		$this->getDataField()->setAllowedExport($allowed);
        return $this;
    }
    
    /**
     * @return int
     */
    public function getAllowedExport() {
        return $this->allowedExport;
    }

	public function addSummary($summaryTitle, $summaryFieldList) {
        $this->summaryTitle = $summaryTitle;
        $this->summaryFieldList = $summaryFieldList;
		$this->getDataField()->addSummary($summaryTitle, $summaryFieldList);
		return $this;
    }
    
    public function removeSummary() {
        $this->summaryTitle = null;
        $this->summaryFields = null;
		$this->getDataField()->addSummary(null, null);
		return $this;
    }
    
        /**
     * Set the sorting of records.
     * 
     * @param int $sort
     */
    public function setSourceSort($sort) {
        $this->sourceSort = $sort;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getSourceSort() {
        return $this->sourceSort;
    }
}